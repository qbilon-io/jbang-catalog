//SOURCES nested/Second.java
//SOURCES nested/Third.java
//SOURCES nested/Fourth.java

package io.qbilon.jbang;

import io.qbilon.jbang.nested.Fourth;
import io.qbilon.jbang.nested.Second;
import io.qbilon.jbang.nested.Third;

public class Test {

    public static void main(String[] args) {
        Second s = new Second();
        s.sayHello();
        Third t = new Third();
        t.sayHello();
        Fourth f = new Fourth();
        f.sayHello();
    }
    
}
